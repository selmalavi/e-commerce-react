import { useState } from 'react';
import './App.css';
import Header from './Component/Header/Header';
import Product from './Component/Product/Product';

function App() {

  const [products,setProducts] = useState([
    {
      image: "https://imagescdn.reebok.in/img/app/product/9/910352-11245888.jpg?auto=format&w=418.4375",
      title: "Mens Reebok Nano X3 Training Shoes",
      price: "$12,999"
    },
    {
      image: "https://imagescdn.reebok.in/img/app/product/9/905014-11144779.jpg?auto=format&w=418.4375",
      title: "Reebok Mens Marco M Training",
      price: "$4,299.00"
    },
    {
      image: "https://imagescdn.reebok.in/img/app/product/9/905011-11144680.jpg?auto=format&w=418.4375",
      title: "Reebok Mens FLUXLITE Training",
      price: "$5,599"
    },
    {
      image: "https://imagescdn.reebok.in/img/app/product/9/905006-11144516.jpg?auto=format&w=418.4375",
      title: "Reebok Mens Marco M Training",
      price: "$4,299.00"
    },
    {
      image: "https://imagescdn.reebok.in/img/app/product/9/905001-11144350.jpg?auto=format&w=418.4375",
      title: "Reebok Mens NANO X2 TR ADVENTURE Traini...",
      price: "$15,999"
    },
    {
      image: "https://imagescdn.reebok.in/img/app/product/9/904997-11144219.jpg?auto=format&w=418.4375",
      title: "Reebok Mens NANOFLEX ADVENTURE TR Tra...",
      price: "$6,999 $9,999 (30%)"
    }

  ])
  return (
    <>
 
      <Header/>
      <main>
        <section>
          <h2>TRAINING</h2>
          <ul className='products'>
            {
              products.map((item) => {
                return <Product product={item}/>
              })
            }
        
            
          </ul>
        </section>
      </main>
 
    </>
  );
}

export default App;
