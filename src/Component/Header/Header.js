function Header() {
    return (
        <header>
            <img src='/logo192.png' alt="React logo" />
            <nav>
                <ul className='navList'>
                    <li>
                        <a href='#'>Home</a>
                    </li>
                    <li>
                        <a href='#'>About</a>
                    </li>
                    <li>
                        <a href='#'>Product</a>
                    </li>
                    <li>
                        <a href='#'>Contact</a>
                    </li>
                </ul>
            </nav>
        </header>
    )
}

export default Header;